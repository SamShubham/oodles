import React from 'react';
import logo from './logo.svg';
import './App.css';
import Home from "./Home";
import UserData from './components/UserData';
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom'; 


function App() {
  return (
    <Router>
    <div className="App">
    <Switch>
      <Route path='/' exact component={Home} />
      <Route path="/UserData" component={UserData} />
      </Switch>
    </div>
    </Router>
  );
}

export default App;