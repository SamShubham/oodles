import React from 'react';

class BannerText extends React.Component{
	render(){
		return(
			<div className="ban_tex_sec">
				<p>We have <span>200,000</span> great job offers you deserve!</p>
				<h1>Get Your Dream Job</h1>
			</div>
		)
	}
}
export default BannerText;