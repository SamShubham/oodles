import React from 'react';
import Form from './Form.js';
import BannerText from './BannerText.js';

class Banner extends React.Component{
	constructor(){
		super();
		this.state={
			fjob:true,
			fcan:false

		}
	}

	render(){
		return(
			<section className="ban_sec">
				<div className="container">
					<div className="col_md_6">
						<BannerText />
						<ul className="ban_btn_list">
							<li><button onClick={()=>{this.setState({fjob:true,fcan:false})}} className={` ${this.state.fjob ? 'active' :'deactive'}`}>Find a Job</button></li>
							<li><button onClick={()=>{this.setState({fjob:false,fcan:true})}} className={`${this.state.fcan ? 'active' :'deactive' }`}>Find a Candidate</button></li>
						</ul>
						<div className={`${this.state.fcan ? 'round_cur' :'deactive' }`}>
							<Form />
						</div>
					</div>
				</div>
			</section>
		)
	}
}
export default Banner;