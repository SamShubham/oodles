import React from 'react';


class UserData extends React.Component{
	constructor(){
		super();
		this.state={
			data:[]
		}
	}
	componentDidMount(){

		fetch('https://api.github.com/users/andrew/repos').then((data)=>{
			data.json().then((resp)=>{
				console.warn('resp',resp);
				this.setState({data:resp})
				console.log(this.state.data[0].name);
						
			})
		})
		console.log(this.state.data);
	}
	render(){
		return(
			<div className="data_page">
				<h1>Shubham <span><img className="user_avter_icon" src="../../logo192.png" /></span></h1>
				<div >
					<ul>
						{ this.state.data.map((item, i) => {

							let statusClass = 'sign'
							if (item.signStatus === 'Out for signature') statusClass += ' sign-complete'
						
							return <li className='item'>
								<div className="user_block">
									<div className="user">
										<p><strong>{item.name}</strong></p>
										<p><em>{item.owner.repos_url}</em></p>
										<p class="lang_color">{item.language}</p>
									</div>
									<div class="dus_box">
										<i className="fa fa-trash" aria-hidden="true"></i>
									</div>
								</div>
							</li>
		      			  })		
		     			}
					</ul>
				</div>
			</div>


		)
	}
}
export default UserData;