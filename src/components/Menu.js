import React from 'react';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom' ;

class Menu extends React.Component{
	constructor(){
		super();
		this.state={
			menu:false
		}
	}
	render(){
		return(
			<div className="main_menu">
				<div className="container">
					<div className="logo_sec">
						<Router>
							<p><Link to="">Logo</Link></p>
						</Router>
					</div>
					<button onClick={()=>{this.setState({menu:!this.state.menu})}}>{this.state.menu ?
						<i class="fa fa-times" aria-hidden="true"></i>
					  :<i class="fa fa-bars" aria-hidden="true"></i>
					}
					</button>
					<div className="menu_sec">
						<Router>
							<ul className={` ${this.state.menu ? 'menu_link active' :'menu_link '}`}>
								<li className="active"><Link to="">Home</Link></li>
								<li><Link to="/about">About</Link></li>
								<li><Link to="/conditates">Conditates</Link></li>
								<li><Link to="/blog">Blog</Link></li>
								<li><Link to="/contact">Contact</Link></li>
							</ul>
							<ul className="menu_btn_link">
								<li><Link  to="./post-job">Post a Job</Link></li>
								<li><Link to="./want-job">Want a Job</Link></li>
							</ul>
						</Router>
					</div>
				</div>
			</div>
		)
	}
}
export default Menu;