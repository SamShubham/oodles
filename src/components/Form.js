import React from 'react';

class Form extends React.Component{
	render(){
		return(
			<div className="form_sec">
				<form>
					<div className="job_tit">
						<input type="text" name="job-tit" placeholder="eg. Garphic. We" />
					</div>
					<div className="job_cat">
						<input type="text" name="job-cat" placeholder="Category" />
					</div>
					<div className="job_loc">
						<input type="text" name="job-loc" placeholder="Location" />
					</div>
					<div className="sub">
						<input type="submit" name="submit" value="Submit"/>
					</div>
				</form>
			</div>
		)
	}
}
export default Form;