import React from 'react';
import Menu from './components/Menu.js';
import Banner from './components/Banner.js';

function Home() {
  return (
    <div className="App">
      <Menu />
      <Banner />
    </div>
  );
}

export default Home;